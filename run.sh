#!/bin/bash

echo '=== Waiting for DB ==='
sleep 10

if [ ! -d "./migrations" ]; then
	echo '=== Preparing DB ==='
	python manage.py db init
	python manage.py db migrate
	python manage.py db upgrade
	python create_user.py
fi

echo '=== Run APP ==='
#exec python main.py
exec gunicorn --bind=0.0.0.0:8088 --workers=4 main:app
