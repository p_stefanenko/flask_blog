# Creating user

from os import environ

from app import db, user_datastore


EMAIL = environ.get('EMAIL', 'admin@example.com')
PASSWORD = environ.get('PASSWORD', 'admin')

role_admin = user_datastore.create_role(name='admin', description='Administrator')
db.session.commit()

user_admin = user_datastore.create_user(email=EMAIL, password=PASSWORD)
db.session.commit()

user_datastore.add_role_to_user(user=user_admin, role=role_admin)
db.session.commit()