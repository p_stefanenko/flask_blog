from flask import Flask
from flask import redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_security import SQLAlchemyUserDatastore, Security
from flask_login import current_user

from config import Configuration

app = Flask(import_name=__name__)
app.config.from_object(obj=Configuration)

db = SQLAlchemy(app=app)

migrate = Migrate(app=app, db=db)
manager = Manager(app=app)
manager.add_command('db', MigrateCommand)

# Admin
from models import Post, Tag


class AdminMixin:
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login', next=request.url))


class HomeAdminIndexView(AdminMixin, AdminIndexView):
    pass


class BaseModelView(ModelView):
    def on_model_change(self, form, model, is_created):
        model.generate_slug()
        return super(BaseModelView, self).on_model_change(form, model, is_created)


class PostModelView(AdminMixin, BaseModelView):
    form_columns = ['title', 'body', 'tags']


class TagModelView(AdminMixin, BaseModelView):
    form_columns = ['name', 'posts']


admin = Admin(app=app, name='FlaskApp', url='/', index_view=HomeAdminIndexView(name='Home'))
admin.add_view(view=PostModelView(model=Post, session=db.session))
admin.add_view(view=TagModelView(model=Tag, session=db.session))

# Flask security
from models import User, Role

user_datastore = SQLAlchemyUserDatastore(db=db, user_model=User, role_model=Role)
security = Security(app=app, datastore=user_datastore)


# Jinja2
@app.template_filter('reverse')
def reverse_filter(s):
    return s[::-1]

@app.context_processor
def utility_processor():
    def format_price(amount, currency=u'€'):
        return u'{0:.2f}{1}'.format(amount, currency)
    return dict(format_price=format_price)
