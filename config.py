from os import environ


class Configuration(object):
    DEBUG = environ.get('DEBUG', False)
    SQLALCHEMY_DATABASE_URI = environ.get(
        'DATABASE_URI',
        'postgresql://flask_user:flask_user@127.0.0.1/flask_user'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = environ.get('SECRET_KEY', 'some secret key')

    # Flask security
    SECURITY_PASSWORD_SALT = 'salt 123'
    SECURITY_PASSWORD_HASH = 'sha512_crypt'
