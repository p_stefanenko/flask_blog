from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, validators

class PostForm(FlaskForm):
    title = StringField('Title', validators=[validators.InputRequired(), validators.Length(min=5, max=140)])
    body = TextAreaField('Body', validators=[validators.InputRequired(), validators.Length(min=5)])
