from flask import Blueprint, render_template, request, redirect, url_for
from flask_security import login_required

from models import Post, Tag
from app import db

from .forms import PostForm


posts = Blueprint(
    name='posts',
    import_name=__name__,
    template_folder='templates'
)


# http://localhost/blog/create
@posts.route('/create', methods=['POST', 'GET'])
@login_required
def create_post():
    if request.method == 'POST':
        form = PostForm(formdata=request.form)
        if form.validate():
            try:
                post = Post(title=form.title.data, body=form.body.data)
                db.session.add(post)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                print(f'Post creating error: {e}')

            return redirect(location=url_for('posts.index'))

        return render_template(template_name_or_list='posts/create_post.html', form=form)

    form = PostForm()
    return render_template(template_name_or_list='posts/create_post.html', form=form)


# http://localhost/blog/first-post/edit
@posts.route('/<path:slug>/edit', methods=['POST', 'GET'])
@login_required
def edit_post(slug):
    post = Post.query.filter(Post.slug==slug).first_or_404()

    if request.method == 'POST':
        form = PostForm(formdata=request.form, obj=post)
        form.populate_obj(post)
        db.session.commit()
        return redirect(url_for(endpoint='posts.post_details', slug=post.slug))

    form = PostForm(obj=post)
    return render_template(template_name_or_list='posts/edit_post.html', post=post, form=form)


# http://localhost/blog/
@posts.route('/')
def index():
    page = request.args.get('page')
    if page and page.isdigit():
        page = int(page)
    else:
        page = 1

    q = request.args.get('q')
    if q:
        posts = Post.query.filter(Post.title.contains(q) | Post.body.contains(q)).order_by(Post.created_at.desc())
    else:
        posts = Post.query.order_by(Post.created_at.desc())

    pages = posts.paginate(page=page, per_page=5)
    return render_template(template_name_or_list='posts/index.html', pages=pages)


# http://localhost/blog/first-post
@posts.route('/<path:slug>')
def post_details(slug):
    post = Post.query.filter(Post.slug == slug).first_or_404()
    tags = post.tags
    return render_template(template_name_or_list='posts/post_details.html', post=post, tags=tags)


# http://localhost/blog/tag/first-tag
@posts.route('/tag/<path:slug>')
def tag_details(slug):
    tag = Tag.query.filter(Tag.slug == slug).first_or_404()
    posts = tag.posts.all()
    return render_template(template_name_or_list='posts/tag_details.html', tag=tag, posts=posts)
