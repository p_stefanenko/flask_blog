--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.1 (Debian 13.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO flask_user;

--
-- Name: post; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public.post (
    id integer NOT NULL,
    title character varying(140),
    slug character varying(140),
    body text,
    created_at timestamp without time zone
);


ALTER TABLE public.post OWNER TO flask_user;

--
-- Name: post_id_seq; Type: SEQUENCE; Schema: public; Owner: flask_user
--

CREATE SEQUENCE public.post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_id_seq OWNER TO flask_user;

--
-- Name: post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flask_user
--

ALTER SEQUENCE public.post_id_seq OWNED BY public.post.id;


--
-- Name: post_tag; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public.post_tag (
    post_id integer,
    tag_id integer
);


ALTER TABLE public.post_tag OWNER TO flask_user;

--
-- Name: role; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public.role (
    id integer NOT NULL,
    name character varying(100),
    description character varying(255)
);


ALTER TABLE public.role OWNER TO flask_user;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: flask_user
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO flask_user;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flask_user
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: tag; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public.tag (
    id integer NOT NULL,
    name character varying(100),
    slug character varying(100)
);


ALTER TABLE public.tag OWNER TO flask_user;

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: flask_user
--

CREATE SEQUENCE public.tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO flask_user;

--
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flask_user
--

ALTER SEQUENCE public.tag_id_seq OWNED BY public.tag.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(100),
    password character varying(255),
    active boolean
);


ALTER TABLE public."user" OWNER TO flask_user;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: flask_user
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO flask_user;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flask_user
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: users_roles; Type: TABLE; Schema: public; Owner: flask_user
--

CREATE TABLE public.users_roles (
    user_id integer,
    role_id integer
);


ALTER TABLE public.users_roles OWNER TO flask_user;

--
-- Name: post id; Type: DEFAULT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.post ALTER COLUMN id SET DEFAULT nextval('public.post_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: tag id; Type: DEFAULT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.tag ALTER COLUMN id SET DEFAULT nextval('public.tag_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public.alembic_version (version_num) FROM stdin;
50c473785fec
\.


--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public.post (id, title, slug, body, created_at) FROM stdin;
1	First post title	first-post-title	First post body	2021-01-15 12:36:17.797618
2	Second post title	second-post-title	Second post body	2021-01-15 12:36:17.797618
3	Third title! 3-test	third-title--3-test	Third post body	2021-01-15 12:36:17.797618
5	5 post	5-post	5 post body	2021-01-17 17:59:06.882557
6	6 post title	6-post-title	6 post body	2021-01-17 21:14:08.874256
7	7 post title	7-post-title	7 post body	2021-01-17 21:14:08.874256
8	8 post title	8-post-title	8 post body	2021-01-17 21:14:08.874256
11	11 post title	11-post-title	11 post body	2021-01-17 21:14:08.874256
9	9 post title v2	9-post-title	9 post body v2	2021-01-17 21:14:08.874256
10	10 post title v2	10-post-title	10 post body v2	2021-01-17 21:14:08.874256
4	Four post	four-post	Four post body	2021-01-17 17:55:41
13	some post 2	some-post-2	body	2021-01-19 22:38:40
15	new post title	new-post-title	body	2021-01-20 11:55:10.812536
\.


--
-- Data for Name: post_tag; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public.post_tag (post_id, tag_id) FROM stdin;
1	1
2	2
4	2
13	1
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public.role (id, name, description) FROM stdin;
1	admin	administrator
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public.tag (id, name, slug) FROM stdin;
1	python	python
2	flask	\N
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public."user" (id, email, password, active) FROM stdin;
1	admin@example.com	$6$rounds=656000$Ce5zjvutUFcZ8/iL$MBWMkxoYgi5XL9zoI3PaXxLR2k3IAszBHnfq1BfxcTBZ/fPNTL5KfrRzmFJyxseeeKT6hSw5D0TQjjef1rlDC/	t
\.


--
-- Data for Name: users_roles; Type: TABLE DATA; Schema: public; Owner: flask_user
--

COPY public.users_roles (user_id, role_id) FROM stdin;
1	1
\.


--
-- Name: post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flask_user
--

SELECT pg_catalog.setval('public.post_id_seq', 15, true);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flask_user
--

SELECT pg_catalog.setval('public.role_id_seq', 1, true);


--
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flask_user
--

SELECT pg_catalog.setval('public.tag_id_seq', 2, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flask_user
--

SELECT pg_catalog.setval('public.user_id_seq', 1, true);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: post post_pkey; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);


--
-- Name: post post_slug_key; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_slug_key UNIQUE (slug);


--
-- Name: role role_name_key; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_name_key UNIQUE (name);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- Name: tag tag_slug_key; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_slug_key UNIQUE (slug);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: post_tag post_tag_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.post_tag
    ADD CONSTRAINT post_tag_post_id_fkey FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: post_tag post_tag_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.post_tag
    ADD CONSTRAINT post_tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- Name: users_roles users_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT users_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- Name: users_roles users_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: flask_user
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT users_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

