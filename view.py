from flask import render_template, flash

from app import app


@app.route('/')
def index():
    flash(message='Some flash message!', category='message')
    flash(message='Flash new message!', category='error')
    return render_template(
        template_name_or_list='index.html',
        name='Павел'
    )


@app.errorhandler(404)
def page_not_found(e):
    return render_template(template_name_or_list='404.html'), 404
